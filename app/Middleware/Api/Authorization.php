<?php
  namespace Omtech\Example\Middleware\Api;

  use Omtech\Routing\Request;
  use Omtech\Example\Oauth2;

  class Authorization {
    protected $storage;
    protected $server;

    public function handle(Request $request, $next) {
      header('Access-Control-Allow-Origin: *');
      header('Access-Control-Allow-Methods: GET');
      header('Access-Control-Allow-Credentials: true');
      header('Access-Control-Allow-Headers: Content-Type, authorization, X-Requested-With');
      header('Content-Type: application/json');

      if ($request->method != "OPTIONS" && !Oauth2::Server()->verifyResourceRequest(\OAuth2\Request::createFromGlobals())) {
        Oauth2::Server()->getResponse()->send();
        return false;
      } else {
        return $next($request);
      }
    }


  }
