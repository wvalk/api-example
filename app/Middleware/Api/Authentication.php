<?php
  namespace Omtech\Example\Middleware\Api;

  use Omtech\Routing\Request;

  class Authentication {
    protected $auth_protocol = 'http';
    protected $auth_server = 'auth.cs.loc:8080';
    protected $auth_clientId = 'api.cs.loc';
    protected $auth_redirectUri = 'http://api.cs.loc:8080/oauth2';

    public function handle(Request $request, $next) {
      if(session_get('access_token') == null)
        return redirect($this->auth_protocol.'://'.$this->auth_server.'/oauth2/authorize?response_type=token&client_id='.$this->auth_clientId.'&state=xyz&redirect_uri='.$this->auth_redirectUri);

      return $next($request);
    }


  }
