<?php
  namespace Omtech\Example\Middleware\Auth;

  use Omtech\Routing\Request;

  class Authentication {

    public function handle(Request $request, $next) {
      if(session_get('username') === false) {
        session_set('req_uri', $request->req_uri);
        return redirect(route('login'));
      } else {
        return $next($request);
      }
    }


  }
