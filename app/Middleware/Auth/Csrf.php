<?php
  namespace Omtech\Example\Middleware\Auth;

  use Omtech\Routing\Request;

  class Csrf {

    public function handle(Request $request, $next) {
      return $next($request);
    }


  }
