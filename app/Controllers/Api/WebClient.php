<?php
  namespace Omtech\Example\Controllers\Api;

  class WebClient {

    public function __construct() {

    }

    public function Home() {
      $access_token = session_get('access_token');
      $expires_at = session_get('access_token_expires_at');
      return view('Api.WebClient', ['access_token' => $access_token, 'expires_at' => $expires_at]);
    }

  }
