<?php
namespace Omtech\Example\Controllers\Api;

use Omtech\Routing\Request;

class Oauth2Controller {

  public function __construct() {

  }

  public function Fetch() {
    return view('Api.Oauth2');
  }

  public function Post(Request $request) {
    $access_token = $request->input('access_token');
    $expires_in = $request->input('expires_in');
    $expires_at = $request->input('expires_at');

    if (empty($access_token))
      return response('Access token not provided', 500);

    if (!$this->checkAccessToken($access_token))
      return response('Invalid access token provided', 500);

    session_set('access_token', $access_token);
    session_set('access_token_expires_at', $expires_at);
    return Json([
      'access_token'  => $access_token,
      'expires_in'    => $expires_in,
      'expires_at'    => $expires_at
    ]);
  }

  private function CheckAccessToken($access_token) {
    return true;
  }

  public function Signout() {
    session_null('access_token');
    session_null('access_token_expires_at');
    return response('U are logged out');
  }

}
