<?php

namespace Omtech\Example\Controllers\Api;

use Omtech\Example\Models\Auth\User;
use Omtech\Routing\Request;
use Omtech\Example\Oauth2;

class ProfileController {
  protected $server;
  protected $storage;

  public function GetProfile(Request $request) {
    $token = Oauth2::Server()->getAccessTokenData(\OAuth2\Request::createFromGlobals());
    $username = $token['user_id'];
    $user = User::find($username);
    return Json($user);
  }


}
