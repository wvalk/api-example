<?php

namespace Omtech\Example\Controllers\Auth;

use Omtech\Routing\Request;
use Omtech\Example\Models\Auth\User;
use Omtech\Example\Models\Auth\AccessToken;
use Omtech\Example\Models\Auth\RefreshToken;

class AccountController {
  protected $user;
  protected $client_id = "api.cs.loc";
  protected $client_pw = "OGFmMTUxZWExMzIxMjM3NzEwNTlmYzZlMzlkNTJmZmE=";
  protected $auth_srv = "http://auth.cs.loc:8080/oauth2/token";

  public function Signin() {
    if(session_get('username') != null)
      return redirect(route('home'));
    return view('Auth.signin');
  }

  public function Login(Request $request) {
    if(session_get('username') !== false)
      return redirect(route('home'));

    /*

      Insert more authentication types here

    */

    if($this->LocalAuth($request->input('username'), $request->input('password'))) {
      session_set('username', $this->user->username);
    }

    if(session_get('username') === false) {
      return view('Auth.signin', ['error' => 'Invalid username and/or password']);
    }

    if(session_get('req_uri') !== false)
      return redirect(session_get('req_uri'));

    return redirect(route('home'));
  }

  private function LocalAuth($username, $password) {
    if(empty($username) || empty($password))
      return false;

    $this->user = User::find($username);
    if(!isset($this->user) || !password_verify($password, $this->user->password))
      return false;

    return true;
  }

  private function getAccessToken() {

    $ch = curl_init($this->auth_srv);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_USERPWD, $this->client_id.":".$this->client_pw);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

    $result['body'] = curl_exec($ch);
    if($result['body'] === false)
      throw new \Exception('Error during authentication process to server: '.$this->auth_srv."\n Error: ".curl_error($ch));

    $result['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if(!$result['http_code'] == 200)
      throw new \Exception('Error during authentication process: response code:' . $result['http_code']);

    $json = json_decode($result['body']);
    if(!$json->access_token)
      throw new \Exception('Error during authentication process: response data: ' . $result['body']);

    session_set('access_token', $json->access_token);
  }

  public function Logout(Request $request) {
    if(session_get('username') === false)
      return redirect(route('login'));

    //$this->cleanup();

    session_destroy();
    if($request->input('req_uri') !== false)
      return redirect(urldecode($request->input('req_uri')));

    return redirect(route('login'));
  }

  private function cleanup() {
    $access_tokens = AccessToken::where('user_id', session_get('username'))->get();
    foreach($access_tokens as $access_token) {
      $access_token->delete();
    }

    $refresh_tokens = RefreshToken::where('user_id', session_get('username'))->get();
    foreach($refresh_tokens as $refresh_token) {
      $refresh_token->delete();
    }
  }

  public function Signup() {

  }

  public function Register(request $request) {

  }

  public function Home() {
    $username = session_get('username');
    $access_tokens = AccessToken::where('user_id', session_get('username'))->get();
    $refresh_tokens = RefreshToken::where('user_id', session_get('username'))->get();
    $info = [
      'username' => $username,
      'access_tokens' => $access_tokens,
      'refresh_tokens' => $refresh_tokens
    ];
    return response(JSON($info));
  }

}
