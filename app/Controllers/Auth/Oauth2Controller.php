<?php

namespace Omtech\Example\Controllers\Auth;

use Omtech\Routing\Request;
use Omtech\Example\Oauth2;

class Oauth2Controller {
  private $storage;
  private $server;

  public function __construct() {
    /*$connection = \Illuminate\Database\Capsule\Manager::Connection()->getConfig();
    $dsn      = $connection['driver'].":dbname=".$connection['database'].";host=".$connection['host'];

    $this->storage = new \OAuth2\Storage\Pdo(array('dsn' => $dsn, 'username' => $connection['username'], 'password' => $connection['password']));
    $this->server = new \OAuth2\Server($this->storage,[
      'allow_implicit'  => true,
    ]);

    $this->server->addGrantType(new \OAuth2\GrantType\RefreshToken($this->storage));
    $this->server->addGrantType(new \OAuth2\GrantType\UserCredentials($this->storage));
    $this->server->addGrantType(new \OAuth2\GrantType\AuthorizationCode($this->storage));
    $this->server->addGrantType(new \OAuth2\GrantType\ClientCredentials($this->storage));
    $this->server->addGrantType(new \OAuth2\GrantType\JwtBearer($this->storage, 'http://auth.cs.loc'));
    */
  }

  public function Token() {
    // Handle a request for an OAuth2.0 Access Token and send the response to the client
    //$this->server->handleTokenRequest(\OAuth2\Request::createFromGlobals())->send();
    Oauth2::Server()->handleTokenRequest(\OAuth2\Request::createFromGlobals())->send();
  }

  public function Authorize(Request $request) {
    $oauth2request = \OAuth2\Request::createFromGlobals();
    $response = new \OAuth2\Response();

    // validate the authorize request
    if (!Oauth2::Server()->validateAuthorizeRequest($oauth2request, $response)) {
        $response->send();
        die;
    }

    // display an authorization form
    /*if (empty($_POST)) {
      exit('
    <form method="post">
      <label>Do You Authorize TestClient?</label><br />
      <input type="submit" name="authorized" value="yes">
      <input type="submit" name="authorized" value="no">
    </form>');
    }*/

    // print the authorization code if the user has authorized your client
    //$is_authorized = ($_POST['authorized'] === 'yes');
    $userid = session_get('username');
    Oauth2::Server()->handleAuthorizeRequest($oauth2request, $response, true, $userid);
    $response->send();
  }

  public function Resource() {
    if (!Oauth2::Server()->verifyResourceRequest(\OAuth2\Request::createFromGlobals())) {
      Oauth2::Server()->getResponse()->send();
      die;
    }
    $token = Oauth2::Server()->getAccessTokenData(\OAuth2\Request::createFromGlobals());
    echo json_encode(array('success' => true, 'message' => 'You accessed my APIs!', 'user_id' => $token['user_id']));
  }

}
