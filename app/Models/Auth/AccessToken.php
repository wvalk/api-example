<?php

  namespace Omtech\Example\Models\Auth;

  use Illuminate\Database\Eloquent\Model;

  class AccessToken extends model {

    protected $table = 'oauth_access_tokens';
    protected $primaryKey = 'access_token'; // or null
    public $incrementing = false;

  }
