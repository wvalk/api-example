<?php

  namespace Omtech\Example\Models\Auth;

  use Illuminate\Database\Eloquent\Model;

  class RefreshToken extends model {

    protected $table = 'oauth_refresh_tokens';
    protected $primaryKey = 'refresh_token'; // or null
    public $incrementing = false;

  }
