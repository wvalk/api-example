<?php

namespace Omtech\Example\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
  protected $table = 'oauth_users';
  protected $primaryKey = 'username';
  public $incrementing = false;
  protected $fillable = [
    'username',
    'first_name',
    'last_name',
    'email',
    'birthdate',
    'gender'
  ];
  protected $hidden = [
    'password'
  ];


}
