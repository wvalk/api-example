<?php

namespace Omtech\Example;

class Oauth2 {
  private static $initialized;
  private static $server;
  private static $storage;

  private static function initialize() {
    if (self::$initialized)
      return;

    $connection = \Illuminate\Database\Capsule\Manager::Connection()->getConfig();
    switch ($connection['driver']) {
      case 'sqlite':
        $dsn      = $connection['driver'].":".$connection['database'];
        break;
      default:
        $dsn      = $connection['driver'].":dbname=".$connection['database'];
        if(!empty($connection['host']))
          $dsn .= ";host=".$connection['host'];
        break;
    }


    self::$storage = new \OAuth2\Storage\Pdo(array('dsn' => $dsn, 'username' => $connection['username'], 'password' => $connection['password']));
    self::$server = new \OAuth2\Server(self::$storage,[
      'allow_implicit'  => true,
    ]);

    self::$server->addGrantType(new \OAuth2\GrantType\RefreshToken(self::$storage));
    self::$server->addGrantType(new \OAuth2\GrantType\UserCredentials(self::$storage));
    self::$server->addGrantType(new \OAuth2\GrantType\AuthorizationCode(self::$storage));
    self::$server->addGrantType(new \OAuth2\GrantType\ClientCredentials(self::$storage));
    self::$server->addGrantType(new \OAuth2\GrantType\JwtBearer(self::$storage, 'http://auth.cs.loc'));

    self::$initialized = true;
  }

  public static function Server() {
    self::initialize();
    return self::$server;
  }


}
