<?php

  $router->group(['domain' => 'auth.cs.loc', 'namespace' => 'Omtech\Example\Controllers\Auth'], function() use($router){
    $router->group(['middleware' => ['Auth\\Authentication']], function() use($router){
      $router->get(['', 'home'], 'AccountController@Home');
    });

    $router->group(['middleware' => ['Auth\\Csrf']], function() use($router){
      $router->get(['signin', 'login'], 'AccountController@Signin');
      $router->post('signin', 'AccountController@Login');
      $router->get('signout', 'AccountController@Logout');
      $router->get('register', 'AccountController@Signup');
      $router->post('register', 'RegController@Register');
    });


    $router->group(['prefix' => 'oauth2'], function() use($router){
      $router->group(['middleware' => ['Auth\\Authentication']], function() use($router) {
        $router->get('authorize', 'Oauth2Controller@Authorize');
        $router->post('authorize', 'Oauth2Controller@Authorize');
      });

      $router->post('token', 'Oauth2Controller@Token');

    });


    /*$router->group(['prefix' => 'api'], function() use($router){

    });*/

  });

  $router->group(['domain' => 'api.cs.loc',  'namespace' => 'Omtech\Example\Controllers\Api'], function() use($router){
    if(!isbrowser()) {
      $router->get('', function(){
        return response('Unauthtorized', 403);
      });
    } else {
      $router->group(['middleware' => ['Api\\Authentication']], function() use($router){
        $router->get('', 'WebClient@Home');
      });
      $router->get('oauth2', 'Oauth2Controller@Fetch');
      $router->post('oauth2', 'Oauth2Controller@Post');
      $router->get('signout', 'Oauth2Controller@Signout');
    }

    $router->group(['middleware' => ['Api\\Authorization']], function() use($router){
      $router->group(['prefix' => 'profile'], function() use($router){
        $router->get('', 'ProfileController@GetProfile');
        $router->get('{any}', 'ProfileController@GetProfile');
      });
    });

  });
