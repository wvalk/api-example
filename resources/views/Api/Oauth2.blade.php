<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>OneChat Login - Oauth Callback</title>
    <script>
      var callbackResponse = (document.URL).split("#")[1];
      console.log(callbackResponse);
      var responseParameters = (callbackResponse).split("&");
      console.log(responseParameters);
      var parameterMap = [];
      for(var i = 0; i < responseParameters.length; i++) {
          parameterMap[responseParameters[i].split("=")[0]] = responseParameters[i].split("=")[1];
      }
      if(parameterMap.access_token !== undefined && parameterMap.access_token !== null) {
        var d = new Date();
        var time = d.getTime();
        var auth = {
                access_token: parameterMap.access_token,
                expires_in: parameterMap.expires_in,
                expires_at: time + (parameterMap.expires_in*1000),
        };
        var http = new XMLHttpRequest();
        http.open("POST", (document.URL).split("#")[0], true);
        http.setRequestHeader("Content-type", "application/json");
        http.onreadystatechange = function() {
          if(http.readyState == 4 && http.status == 200) {
            window.location.href = '/';
          } else if (http.readyState == 4) {
            alert("Problem authenticating");
          }
        }
        http.send(JSON.stringify(auth));
      } else {
        alert("Problem authenticating");
      }
    </script>
  </head>
  <body>
    <md-content>Redirecting...</md-content>
  </body>
</html>
