<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Example App Signin</title>
    <script src="js/jquery-1.11.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #eee;
      }

      .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
      }

      .form-signin .form-signin-heading {
        margin-top: 10px;
        margin-bottom: 20px;
      }

      .form-signin .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
           -moz-box-sizing: border-box;
                box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
      }
      .form-signin .form-control:focus {
        z-index: 2;
      }
      .form-signin input[name="username"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
      }
      .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
      }
    </style>
    <script type="text/javascript">
      $(document).ready(function() {
        if(top.location.href.indexOf('signin') < 0) top.location.replace('/signin');
      });
    </script>
  </head>
  <body>



    <div class="container">
      <form class="form-signin" method="post" autocomplete="off">
        <div class="panel panel-default">
          <div class="panel-body">
            <h2 class="form-signin-heading text-center">Example App Signin</h2>
            @if(isset($error))
            <div class="alert alert-danger">{{$error}}</div>
            @endif
            {! csrf_field() !}
            <label for="inputUsername" class="sr-only">Username</label>
            <input name="username" type="text" id="inputUsername" class="form-control" placeholder="Gebruikersnaam" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Wachtwoord" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
            <br>
            <span><a href="/register">Registreer een nieuw account</a></span><br>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>
