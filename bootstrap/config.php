<?php

  $classMap = [
    'Capsule'   => 'Illuminate\Database\Capsule\Manager',
    'Router'    => 'Omtech\Routing\Router',
    'Helper'    => 'Omtech\Helper\Helper',
    'Resolver'  => 'Omtech\Routing\Resolver',
    'Request'   => 'Omtech\Routing\Request'
  ];

  $globalVars = [
    'DEBUG'             => true,
    'APP_DIR'           => __DIR__.'/../',
    'ROUTE_FILE'        => __DIR__.'/../app/routing.php',
    'APP_NAMESPACE'     => 'Omtech\\Example',
    'DEFAULT_NAMESPACE' => 'Omtech\\Example\\Controllers',
    'VIEW_DIR'          => __DIR__.'/../resources/views/',
    'LANGS_DIR'         => __DIR__.'/../resources/langs/',
    'SESSION_DIR'       => __DIR__.'/../storage/sessions',
    'STORAGE_DIR'       => __DIR__.'/../storage/',
  ];

  $dbconf = [
      'driver'    => 'sqlite',
      'host'      => '',
      'database'  => __DIR__.'/../storage/database.sqlite3',
      'username'  => '',
      'password'  => '',
      'charset'   => 'utf8',
      'collation' => 'utf8_unicode_ci',
      'prefix'    => '',
  ];

  $enckey = "ZTdmYTRkYTM2NDBhYTMwZWMwMjIwZDIzNmJhMmFiMTY=";
