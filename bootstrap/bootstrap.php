<?php

  /*
  |  Require composer autoloader for class mapping
  |  Require config file for app specific settings
  */
  require __DIR__ . '/../vendor/autoload.php';
  require __DIR__ . '/config.php';

  foreach($globalVars as $constant => $value) {
    define($constant, $value);
  }

  foreach($classMap as $allias => $class) {
    class_alias($class, $allias);
  }

  /*
  | Calloud to some usefull simple functions
  */
  Helper::Help();

  /*
  | Create Capsule to support Eloquents
  */
  $capsule = new Capsule;
  $capsule->addConnection($dbconf);
  $capsule->setAsGlobal();
  $capsule->bootEloquent();

  /*
  | Initiate the routing process (build routemap) and save named routes
  */
  $router = new Router;
  require ROUTE_FILE;
  define("NAMED_ROUTES", $router->getnamedRoutes());

  if(@$webserve) {
    /*
    | Enable Sessions
    */
    session_save_path(SESSION_DIR);
    session_start();

    /*
    | Initiate the dispatcher process (return closure or method based on route)
    */
    //return pr($router->getData());
    $resolver = new Resolver(new Request, $router->getData());
  }
