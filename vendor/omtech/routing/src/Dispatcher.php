<?php

  namespace Omtech\Routing;

  class Dispatcher {
    private $request;

    function __construct(Request $request) {
      $this->setErrorHandler();
      if(!empty($request->getMiddleware())) {
        echo $this->callMiddleware($request);
      } else {
        if(is_callable($request->getHandler())) {
          echo $this->callClosure($request->getHandler(), $request->getParms());
        } else {
          echo $this->callMethod($request, $request->getHandler(), $request->getParms());
        }
      }
    }

    function callMiddleware(Request $request) {
      $next = function(Request $request) use (&$next) {
        if(($classname = $request->shiftMiddleware()) !== NULL) {
          $classname = APP_NAMESPACE . '\\Middleware\\' . $classname;
          $reflect = new \ReflectionMethod($classname, 'handle');
          return $reflect->Invoke(new $classname, $request, $next);

        } else {
          if(is_callable($request->getHandler())) {
            $reflect = new \ReflectionFunction($closure);
            if(!empty($request->getParms()))
              return $reflect->Invokeargs($this, $request->getParms());

            return $reflect->Invoke($this);
          } else {
            $classname = explode('@', $request->getHandler())[0];
            $funcname = explode('@', $request->getHandler())[1];
            $reflect = new \ReflectionMethod($classname, $funcname);
            $parms = [$request];
            if(!empty($request->getParms()))
              $parms = array_merge($parms, $request->getParms());
            return $reflect->Invokeargs(new $classname, $parms);
          }
        }
      };
      return call_user_func_array($next, [$request]);
    }

    function callClosure($closure, $parms = []) {
        $reflect = new \ReflectionFunction($closure);
        return $reflect->Invoke($this, $parms);
    }

    function callMethod(Request $request, $handler, $parms) {
      $handler = explode('@', $request->getHandler());
      $classname = $handler[0];
      $funcname = $handler[1];
      $reflect = new \ReflectionMethod($classname, $funcname);
      $parms = [$request];
      if(!empty($request->getParms()))
        $parms = array_merge($parms, $request->getParms());
      return $reflect->Invokeargs(new $classname, $parms);
    }

    function checkForErrors($class) {
      $file = APP_DIR;
      $file .= "app/Controllers/";
      $file .= str_replace('\\', '/', str_replace(DEFAULT_NAMESPACE, "", $class));
      $file .= ".php";
      $status = shell_exec('php -l '.realpath($file).' 2>&1 1> /dev/null');
      if(strpos($status, "No syntax errors") !== false) {
        return false;
      } else {
        return $status;
      }
    }

    function setErrorHandler(){
      set_exception_handler(function($exception){
        if(strpos($exception->getMessage(), "does not exist") !== false) {
          $syntaxerror = $this->checkForErrors($exception->getTrace()[0]['args'][0]);
          if($syntaxerror) {
            error_log($syntaxerror);
          } else {
            error_log($exception->getMessage());
          }
        }
http_response_code(500);
print<<<ENDHTML
<!DOCTYPE html>
<html lang="nl">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Edit</title>
  <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>
  <div class="container">
          <div class="alert alert-danger" style="text-align:center; margin-top: 10px;"><h4>Oops, something went wrong!</h4></div>
ENDHTML;
        $debug = DEBUG;
        if($debug) {
          if(@$syntaxerror) {
            echo '<pre style="white-space: pre-wrap;">';
            echo @$syntaxerror;
            echo '</pre>';
          } else {
            echo @$syntaxerror;
            echo '<pre style="white-space: pre-wrap;">';
            echo 'Error Exception'."\n";
            echo 'in '.$exception->getFile().' line '.$exception->getLine().":\n";
            echo $exception->getMessage() . "\n";

            $trace = $exception->getTrace();
            //array_shift($trace);

            print_r($trace);
            echo '</pre>';
          }
        }
print<<<ENDHTML
  </div>
</body>
</html>
ENDHTML;
      });
    }

  }
