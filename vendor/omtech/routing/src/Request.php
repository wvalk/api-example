<?php

  namespace Omtech\Routing;

  class Request {
    public $hostname;
    public $port;
    public $method;
    public $uri;
    public $req_uri;
    private $middleware = array();
    private $handler;
    private $parms;
    private $inputs_run = 0;
    public $inputs = [];
    public $user_agent;

    public function __construct(){
      $info = explode(':', $_SERVER['HTTP_HOST']);
      $this->hostname = $info[0];
      if(isset($info[1])) {
        $this->port = $info[1];
      } else {
        if(!empty($_SERVER['HTTPS'])) {
          $this->port = '443';
        } else {
          $this->port = '80';
        }
      }

      $this->method = $_SERVER['REQUEST_METHOD'];
      $this->uri = explode('?', ltrim(rtrim($_SERVER['REQUEST_URI'], '/'), '/'), 2)[0];

      $uri = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
      $uri .= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
      $this->req_uri = $uri;

      $this->parse_raw_http_request();

      $this->user_agent = $_SERVER['HTTP_USER_AGENT'];
    }

    public function addMiddleware(array $middleware) {
      $this->middleware = array_merge($this->middleware, $middleware);
    }

    public function shiftMiddleware() {
      return array_shift($this->middleware);
    }

    public function getMiddleware() {
      return $this->middleware;
    }

    public function setHandler($handler){
      $this->handler = $handler;
    }

    public function getHandler() {
      return $this->handler;
    }

    public function setParms(array $parms) {
      $this->parms = $parms;
    }

    public function getParms() {
      return $this->parms;
    }

    public function isAjax() {
      if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        return true;
      } else {
        return false;
      }
    }

    public function isBrowser() {
      $browsers = [
        "Chrome",
        "Firefox",
        "Mozilla",
        "Edge",
        "Safari",
        "Internet Explorer",
        "Opera"
      ];
      foreach($browsers as $browser) {
        if(strpos($this->user_agent, $browser))
          return true;
      }
      return false;
    }

    public function isCors() {
      if(!empty($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
        return true;
      } else {
        return false;
      }
    }

    private function parse_raw_http_request() {
      if($this->method == 'GET') {
        $this->inputs = $_GET;
        return true;
      }elseif(strpos($_SERVER["CONTENT_TYPE"], "form-data") !== false) {
        $this->inputs = $_POST;
        return true;
      }elseif(strpos($_SERVER["CONTENT_TYPE"], "x-www-form-urlencoded") !== false) {
        $input = file_get_contents('php://input');
        $arr = array();

        if(preg_match("/-+.*Boundary/", $input)) {
          $blocks = preg_split("/-+.*Boundary/", $input);

          foreach($blocks as $id => $block) {
            if(empty($block) || strpos($block, "name=") === false)
              continue;

              if (strpos($block, 'application/octet-stream') !== FALSE){
                preg_match("/name=\"([^\"]*)\".*stream[\n|\r]+([^\n\r].*)?$/s", $block, $matches);
              }else{
                preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
              }
              if(isset($matches[1]))
              $arr[$matches[1]] = $matches[2];
          }
        } else {
          parse_str($input, $arr);
        }

        $this->inputs = $arr;
      }
      if(strpos($_SERVER["CONTENT_TYPE"], "application/json") !== false) {
        $this->inputs = (array) json_decode(file_get_contents("php://input"));
        return true;
      }
    }

    public function input($name) {
      if(!boolval($this->inputs_run))
        $this->parse_raw_http_request();

      if(isset($this->inputs[$name]))
        return $this->inputs[$name];

      return false;
    }

    public function allInputs() {
      return $this->inputs;
    }

    public function isPost() {
      return $this->method == "POST";
    }

    public function isGet() {
      return $this->method == "GET";
    }



  }
