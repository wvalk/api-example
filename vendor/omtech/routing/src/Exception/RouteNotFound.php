<?php

  namespace Omtech\Routing\Exception;

  class RouteNotFound extends Exception {
    protected $message = 'Route not found: ';

    public function __construct($uri) {
      header("HTTP/1.0 404 Not Found");
      echo '404';
      $this->message = $this->message . $uri;
    }

  }
