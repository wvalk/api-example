<?php

  namespace Omtech\Routing;

  class Router implements RouterInterface {
    private $staticRoutes = array();
    private $variableRoutes = array();
    private $wildcardRoutes = array();
    private $namedRoutes = array();
    private $prefix;
    private $namespace;
    private $middleware;
    private $domain = "";

    /**
    * Constructor to check if routes file is cached
    */
    public function __construct() {
      if(!file_exists(ROUTE_FILE))
        throw new Exception\FileNotExist(ROUTE_FILE);

      $this->namespace = DEFAULT_NAMESPACE;
    }

    /**
    * Class method to add routes with http methods, url and function or name of method within controller
    *
    * @param string $method
    * @param string $uri
    * @param string $handler
    */
    private function addRoute($method, $uri, $handler) {
      if(!is_callable($handler)) {
        $handler = $this->namespace.'\\'.$handler;
      }

      if(!empty($this->prefix) && !empty($uri)) {
        $uri = $this->prefix.'/'.$uri;
      } elseif(!empty($this->prefix)) {
        $uri = $this->prefix.$uri;
      }

      if(strpos($uri, '{') === false && strpos($uri, '{any}') === false) {
        $arr = ['handler' => $handler];
        if(!empty($this->middleware))
          $arr['middleware'] = $this->middleware;
        $this->staticRoutes[$this->domain][$method][$uri] = $arr;
      } elseif(strpos($uri, '{any}') !== false && empty($method)) {
        $arr = ['handler' => $handler];
        if(!empty($this->middleware))
          $arr['middleware'] = $this->middleware;

        $this->wildcardRoutes[$this->domain]['/^'.str_replace('{any}', '(\/)?(.*)', str_replace('/', '\/', $uri)).'/'] = $arr;
      } elseif(strpos($uri, '{any}') !== false && !empty($method)){
        $arr = ['handler' => $handler];
        if(!empty($this->middleware))
          $arr['middleware'] = $this->middleware;
        $this->variableRoutes[$this->domain][$method]['/^'.str_replace('{any}', '(\/)?(.*)', str_replace('/', '\/', $uri)).'/'] = $arr;
      } else {
        $slugs = array();
        foreach(explode('/{', ltrim($uri, '/')) as $key => $val) {
          if(strpos($val, '}') === false) {
            continue;
          }
          preg_match('/(.+)}/', $val, $matches);
          $slugs[$matches[1]] = $key;
        }
        $arr = ['handler' => $handler, 'slugs' => $slugs];
        if(!empty($this->middleware))
          $arr['middleware'] = $this->middleware;
        //$this->variableRoutes[$this->domain][$method]['/^'.preg_replace("/{.+?}/", "([a-zA-Z0-9]+)", str_replace('/', '\/', $uri)).'$/'] = $arr;
        $this->variableRoutes[$this->domain][$method]['/^'.preg_replace("/{.+?}/", "(.*)", str_replace('/', '\/', $uri)).'$/'] = $arr;
      }
    }

    private function addNamedRoute($uri, $name) {
      $this->namedRoutes[$name] = $uri;
    }

    public function get($details, $handler) {
      if(!is_array($details)) {
        $this->AddRoute('GET', $details, $handler);
        $this->AddRoute('HEAD', $details, $handler);
      } else {
        $this->AddRoute('GET', $details[0], $handler);
        $this->AddRoute('HEAD', $details[0], $handler);
        $this->addNamedRoute($details[0], $details[1]);
      }
    }

    public function post($details, $handler) {
      if(!is_array($details)) {
        $this->AddRoute('POST', $details, $handler);
      } else {
        $this->AddRoute('POST', $details[0], $handler);
        $this->addNamedRoute($details[0], $details[1]);
      }
    }

    public function put($details, $handler) {
      if(!is_array($details)) {
        $this->AddRoute('PUT', $details, $handler);
      } else {
        $this->AddRoute('PUT', $details[0], $handler);
        $this->addNamedRoute($details[0], $details[1]);
      }
    }

    public function patch($details, $handler){
      if(!is_array($details)) {
        $this->AddRoute('PATCH', $details, $handler);
      } else {
        $this->AddRoute('PATCH', $details[0], $handler);
        $this->addNamedRoute($details[0], $details[1]);
      }
    }

    public function delete($details, $handler){
      if(!is_array($details)) {
        $this->AddRoute('DELETE', $details, $handler);
      } else {
        $this->AddRoute('DELETE', $details[0], $handler);
        $this->addNamedRoute($details[0], $details[1]);
      }
    }

    public function options($details, $handler){
      if(!is_array($details)) {
        $this->AddRoute('OPTIONS', $details, $handler);
      } else {
        $this->AddRoute('OPTIONS', $details[0], $handler);
        $this->addNamedRoute($details[0], $details[1]);
      }
    }

    public function any($details, $handler){
      if(!is_array($details)) {
        $this->AddRoute('', $details, $handler);
      } else {
        $this->AddRoute('', $details[0], $handler);
        $this->addNamedRoute($details[0], $details[1]);
      }
    }

    public function group($details, $handler) {
      $namespace = $this->namespace;
      $prefix = $this->prefix;
      $middleware = $this->middleware;
      $domain = $this->domain;
      if(isset($details['namespace'])) {
        $this->namespace = $details['namespace'];
      }
      if(isset($details['prefix'])) {
        $this->prefix = $details['prefix'];
      }
      if(isset($details['middleware'])) {
        $this->middleware = $details['middleware'];
      }
      if(isset($details['domain'])) {
        $this->domain = $details['domain'];
      }
      call_user_func($handler);
      $this->namespace = $namespace;
      $this->prefix = $prefix;
      $this->middleware = $middleware;
      $this->domain = $domain;
    }

    public function getData(){
      return [$this->staticRoutes, $this->variableRoutes, $this->wildcardRoutes];
    }

    public function getnamedRoutes() {
      return $this->namedRoutes;
    }

  }
