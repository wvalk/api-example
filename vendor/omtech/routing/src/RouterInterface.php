<?php

  namespace Omtech\Routing;

  Interface RouterInterface {

    public function get($uri, $handler);

    public function post($uri, $handler);

    public function put($uri, $handler);

    public function patch($uri, $handler);

    public function delete($uri, $handler);

    public function options($uri, $handler);

    public function any($uri, $handler);

  }
