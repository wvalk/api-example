<?php

  namespace Omtech\Routing;
  use Omtech\Routing\Exception\RouteNotFound;

  class Resolver {
    private $staticRoutes = array();
    private $variableRoutes = array();
    private $wildcardRoutes = array();
    private $request;

    function __construct(Request $request, array $routes) {
      $this->staticRoutes = $routes[0];
      $this->variableRoutes = $routes[1];
      $this->wildcardRoutes = $routes[2];
      $this->request = $request;

      try {
        $this->resolve();
        $dispatcher = new Dispatcher($this->request);
      } catch (RouteNotFound $e) {
        error_log($e->getMessage());
      }

    }

    function resolve(){
      if(!$this->findStaticRoute() && !$this->findVariableRoute() && !$this->findWildcardRoute()) {
        throw new Exception\RouteNotFound($_SERVER['REQUEST_URI']);
      } else {
        header('Cache-Control: no-cache');
        return true;
      }
    }

    function getRoutesByDomain($routes, $domain) {
      if(isset($routes[$domain])) {
        return $routes[$domain];
      } elseif(isset($routes[''])) {
        return $routes[''];
      } else {
        return false;
      }
    }

    function getRoutesByMethod($routes, $method) {
      if(isset($routes[$method])) {
        return $routes[$method];
      } elseif(isset($routes[''])) {
        return $routes[''];
      } else {
        return false;
      }
    }

    function getRoutesByUri($routes, $uri) {
      if(isset($routes[$uri])) {
        return $routes[$uri];
      } else {
        return false;
      }
    }

    function findStaticRoute() {
      if(empty($this->staticRoutes))
        return false;

      if(!$routes = $this->getRoutesByDomain($this->staticRoutes, $this->request->hostname))
        return false;

      if(!$routes = $this->getRoutesByMethod($routes, $this->request->method))
        return false;

      if(!$routes = $this->getRoutesByUri($routes, $this->request->uri))
        return false;

      if(isset($routes['middleware']))
        $this->request->addMiddleware($routes['middleware']);

      $this->request->setHandler($routes['handler']);
      return true;
    }

    function findVariableRoute() {
      if(empty($this->variableRoutes))
        return false;

      if(!$routes = $this->getRoutesByDomain($this->variableRoutes, $this->request->hostname))
        return false;

      if(!$routes = $this->getRoutesByMethod($routes, $this->request->method))
        return false;

      foreach($routes as $route => $arr) {
        if(!preg_match($route, $this->request->uri, $matches))
          continue;

        if(isset($arr['middleware']))
          $this->request->addMiddleware($arr['middleware']);

        $this->request->setHandler($arr['handler']);

        $parms = array();
        if(!isset($arr['slugs']))
          return true;

        foreach($arr['slugs'] as $slug => $id) {
          $parms[$slug] = $matches[$id];
        }
        $this->request->setParms($parms);
        return true;
      }
    }

    function findWildcardRoute() {
      if(empty($this->wildcardRoutes))
        return false;

      if(!$routes = $this->getRoutesByDomain($this->wildcardRoutes, $this->request->hostname))
        return false;

      foreach($routes as $route => $arr) {
        if(!preg_match($route, $this->request->uri, $matches))
          continue;

        if(isset($arr['middleware']))
          $this->request->addMiddleware($arr['middleware']);

        $this->request->setHandler($arr['handler']);

        $parms = array();
        if(!isset($matches[2]))
          return true;

        $this->request->setParms(explode('/', $matches[2]));
        return true;
      }
    }

  }
