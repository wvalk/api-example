<?php
  namespace Omtech\Commandline\Commands;

  class Create {
    public $command = "create";
    public $args = [
      "type",
      "name"
    ];
    public $description = "create new type of documents";
    private $name = "";

    public function help() {
      return "Creates the type of document specified in the right folder";
    }

    public function handle($type = null, $name = null) {
      $doctypes = [
        "controller"  =>  "Controllers",
        "middleware"  =>  "Middleware",
        "model"       =>  "Models",
        "command"     =>  "Console/Commands"
      ];

      if(empty($type)) {
        echo "Please specify a type of document to create \n";
        return 0;
      }

      if(!in_array($type, array_keys($doctypes))) {
        echo "Doctype does not exist \n";
        return 0;
      }

      if(!file_exists($sourcefile = __DIR__.'/../Templates/'.$type.'.php')) {
        echo "Doctype does not exist \n";
        return 0;
      }

      if(empty($name)) {
        echo "Please specify a name for the document \n";
        return 0;
      }

      $this->name = $name;
      $filename = APP_DIR.'app/'.$doctypes[$type].'/'.$name.'.php';

      if(file_exists($filename)) {
        echo "$type already exists please specify an other name\n";
        return 0;
      }

      $document = file_get_contents(__DIR__.'/../Templates/'.$type.'.php');
      $newdoc = preg_replace_callback("/{{(.*)}}/", [$this, "evalreplace"], $document);
      file_put_contents($filename, $newdoc);

      echo "Successfully created $type \n";
    }

    private function evalreplace($matches) {
      return eval("return ".$matches[1].";");
    }



  }
