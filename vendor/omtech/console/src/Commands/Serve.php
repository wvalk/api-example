<?php
  namespace Omtech\Commandline\Commands;

  class Serve {
    public $command = "serve";
    public $args = [
      "host"
    ];
    public $description = "Serve the local web server";

    public function handle($host = "localhost:8080") {
      echo "PHP Development Server started at ".date("d M Y H:i:s") ."\n";
      echo "Listening on " . $host . "\n";
      echo "Document root is " . realpath('public') . "\n";
      echo "Press Ctrl-C to quit." . "\n";
      system("php -S ".$host." -t public");
    }

  }
