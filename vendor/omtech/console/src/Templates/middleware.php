<?php
  namespace {{APP_NAMESPACE}}Middleware;

  use Omtech\Routing\Request;

  class {{$this->name}} {

    public function handle(Request $request, $next) {
      return $next($request);
    }


  }
