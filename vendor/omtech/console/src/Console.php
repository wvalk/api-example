<?php
  namespace Omtech\Commandline;

  Class Console {
    private $commandsdir;
    private $commands;
    private $prompt;


    public function __construct(){
      $commandssearch = [
        APP_DIR."/app/Console/Commands" => APP_NAMESPACE.'Console\\Commands\\',
        __DIR__."/Commands" => "Omtech\\Commandline\\Commands\\"
      ];

      foreach($commandssearch as $dir => $namespace) {
        $commands = glob($dir."/*.php");
        foreach($commands as $command) {
          $class = $namespace.basename($command, ".php");
          $command = new $class;
          $this->commands[$command->command] = $command;
        }
      }
    }

    private function Help() {
      foreach($this->commands as $command) {
        echo $command->command . "\t\t" . $command->description;
        echo "\n";
      }
    }

    public function Run() {
      global $argc, $argv;

      $this->fp = fopen('php://stdin', 'r');
      $this->last_line = false;
      if(isset($argv[1]))
        return $this->findRoute(implode(' ',array_slice($argv, 1)));

      while (!$this->last_line) {
        echo "# ";
          $next_line = fgets($this->fp);
          $this->findRoute($next_line);
      }
      echo "\n";
    }

    private function findRoute($command) {
      $args = explode(' ', $command);
      switch ($args[0]) {
        case "exit\n":
        case NULL:
        case "quit\n":
          $this->last_line = true;
          break;

        case "help\n":
        case "help":
          $this->Help();
          break;
        default:
          if(isset($this->commands[$args[0]])) {
            $found = $this->commands[$args[0]];
            $argvs = array_slice($args, 1, count($found->args));
            $arguments = array_combine(array_slice($found->args, 0, count($argvs)), $argvs);
            call_user_func_array(array($found, 'handle'), $arguments);
            return 0;
          }
          echo "error: unknown command: '" . str_replace("\n", "", $command) . "'\n";
          return 1;
          break;
      }
    }

  }
