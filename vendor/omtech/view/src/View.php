<?php

  namespace Omtech\View;

  use Omtech\View\Compiler;

  class View {
    public $output;

    public function __construct($compiledview, Buffer $buffer, $vars = null) {
      if($vars !== null) {
        foreach($vars as $var => $value) {
          $$var = $value;
        }
      }

      ob_start();
      eval("?>".$compiledview);
      $this->output = ob_get_clean();

      while($extends = $buffer->getExtends()) {
        $compiler = new Compiler($extends);
        $compiler->compile();
        ob_start();
        eval("?>".$compiler->getCompiled());
        $this->output = ob_get_clean();
      }
      return $this->output;
    }
  }
