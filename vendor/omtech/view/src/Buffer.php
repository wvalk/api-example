<?php

  namespace Omtech\View;

  class Buffer {
    private $buffer = array();
    private $extends = "";
    private $sections = array();

    public function curSection() {
      end($this->sections);
      $section = current($this->sections);
      reset($this->sections);
      return $section;
    }

    public function saveSection($key = null, $value = null) {
      if($value !== null) {
        if(!isset($this->buffer[$key])) {
          $this->buffer[$key] = $value;
        }else{
          $this->buffer[$key] = $this->buffer[$key] . $value;
        }
      } else {
        if(!array_key_exists($this->curSection(), $this->buffer)) {
          $buffer = ob_get_contents();
          $this->buffer[$this->curSection()] = $buffer;
        }else{
          $buffer = ob_get_contents();
          $this->buffer[$this->curSection()] = $this->buffer[$this->curSection()] . $buffer;
        }
        ob_end_clean();
        array_pop($this->sections);
      }
    }

    public function startSection($section) {
      $this->sections[] = $section;
      ob_start();
    }

    public function extends($view) {
      $this->extends = $view;
    }

    public function include() {

    }

    public function showSection() {
      ob_end_clean();
      if(!isset($this->buffer[$this->curSection()]))
        return false;

      echo $this->buffer[$this->curSection()];
      array_pop($this->sections);
      return true;
    }

    public function showYield($yield) {
      if(!isset($this->buffer[$yield]))
        return false;

      echo $this->buffer[$yield];
      return true;
    }

    public function showConstant($constant) {
      echo constant($constant);
    }

    public function getExtends() {
      if(!isset($this->extends))
        return false;
      $extend = $this->extends;
      unset($this->extends);
      return $extend;
    }

  }
