<?php

  namespace Omtech\View;

  class Compiler {
    private $patterns = array();
    private $filecontent = "";
    private $cached = false;
    private $compiled = "";

    public function __construct($viewname) {
      if(strpos($viewname, '.' === false)) {
        $filename = $viewname.'.blade.php';
      }else{
        $exviewname = explode('.', $viewname);
        end($exviewname);
        $exviewname[key($exviewname)] = $exviewname[key($exviewname)].'.blade.php';
        reset($exviewname);
        $filename = implode('/', $exviewname);
      }

      if(!file_exists(VIEW_DIR.$filename))
        throw new Exception\FileNotExist(VIEW_DIR.$filename);

      $this->filecontent = file_get_contents(VIEW_DIR.$filename);

      $this->patterns = [
        "/@section\((.*?), (.*?)\)/"          => "<?php \$buffer->saveSection(\${1}, \${2}); ?>",
        "/@section.*?\((.*?)\)/"              => "<?php \$buffer->startSection(\${1}) ?>",
        "/@stop/"                             => "<?php \$buffer->saveSection() ?>",
        "/@show/"                             => "<?php \$buffer->showSection() ?>",
        "/@yield.*?\((.*)\)/"                 => "<?php \$buffer->showYield(\${1}); ?>",
        "/@if.*?\((.*)\)/"                    => "<?php if(\${1}): ?>",
        "/@else.*?\((.*)\)/"                  => "<?php else(\${1}): ?>",
        "/@elseif.*?\((.*)\)/"                => "<?php elseif(\${1}): ?>",
        "/@endif/"                            => "<?php endif; ?>",
        "/@foreach.*?\((.*)\)/"               => "<?php foreach(\${1}): ?>",
        "/@endforeach/"                       => "<?php endforeach; ?>",
        "/@for.*?\((.*)\)/"                   => "<?php for(\${1}): ?>",
        "/@endfor/"                           => "<?php endfor; ?>",
        "/@while.*?\((.*)\)/"                 => "<?php while(\${1}): ?>",
        "/@endwhile/"                         => "<?php endwhile; ?>",
        "/@extends.*?\((.*)\)/"               => "<?php \$buffer->extends(\${1}); ?>",
        "/@include.*?\((.*)\)/"               => "<?php \$buffer->include(\${1}) ?>",
        "/{{(.*)}}/"                          => "<?php echo \${1} ?>",
        "/{!(.*)!}/"                          => "<?php \${1} ?>",
        "/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/"  => "\n",
        "/@constant\((.*?)\)/"                => "<?php \$buffer->showConstant(\${1}); ?>",
      ];
    }

    public function compile() {
      $this->compiled = preg_replace(array_keys($this->patterns), array_values($this->patterns), $this->filecontent);
    }

    public function getCompiled() {
      return $this->compiled;
    }


  }
