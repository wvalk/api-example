<?php

  use Omtech\View\Compiler;
  use Omtech\View\View;
  use Omtech\View\Buffer;
  use Illuminate\Encryption\Encrypter;

  function view($viewname, $vars = null) {
    $compiler = new Compiler($viewname);
    $compiler->compile();

    $view = new View($compiler->getCompiled(), new Buffer, $vars);
    return $view->output;
  }

  function lang($translation) {
    $extrans = explode('.', $translation);
    if(!count($extrans) >= 2)
      return false;

    end($extrans);
    prev($extrans);
    $extrans[key($extrans)] = $extrans[key($extrans)].'.php';
    end($extrans);

    $filename = rtrim(implode('/', $extrans), '/'.$extrans[key($extrans)]);

    if(!file_exists(LANGS_DIR.$filename))
      throw new Omtech\Helper\Exception\FileNotExist(LANGS_DIR.$filename);

    $trans = eval('?>' . file_get_contents(LANGS_DIR.$filename));
    return $trans[current($extrans)];
  }

  function route($name) {
    if(isset(NAMED_ROUTES[$name]))
      return '/' . NAMED_ROUTES[$name];

    return false;
  }

  function redirect($uri, $parms = []) {
    $uri = preg_replace_callback('/{(.*?)}/', function($vars) use($parms){
      return $parms[$vars[1]];
    }, $uri);
    ob_start();
    header('Location: '. $uri);
    $ob = ob_get_clean();
    return $ob;
  }

  function response($content, $code = 200) {
    http_response_code($code);
    return $content;
  }

  function json($string) {
    header('Content-type: application/json');
    return json_encode($string);
  }

  function pwhash($string, $algo = PASSWORD_DEFAULT) {
    return password_hash($string, $algo);
  }

  function encrypt($string, $optionalkey = null) {
    require APP_DIR.'bootstrap/config.php';

    $key = $optionalkey ?? $enckey;

    $encryptor = new Encrypter($key, 'AES-256-CBC');
    return $encryptor->encrypt($string);
  }

  function decrypt($string, $optionalkey = null) {
    require APP_DIR.'bootstrap/config.php';

    $key = $optionalkey ?? $enckey;

    $encryptor = new Encrypter($key, 'AES-256-CBC');
    return $encryptor->decrypt($string);
  }

  function table(array $headers, $objects, $pwfield = null) {
    $table = ['headers' => $headers, 'objects' => [], 'tableSchema' => json_encode(array_keys($headers))];
    if(isset($changepw)) {
      $table['pwfield'] = $changepw;
    }
    foreach($objects as $object) {
      if($object instanceof Illuminate\Database\Eloquent\Model) {
        $filterobject = [];
        foreach(array_keys($headers) as $header) {
          $value = null;
          if(strpos($header, '->') !== false) {
            $value = null;
            foreach(explode('->', $header) as $step) {
              if(isset($value)) {
                $value = $value->$step;
              } else {
                $value = $object->$step;
              }
            }
          } else {
            $value = $object[$header];
          }

          $filterobject[$header] = $value;
        }
        $table['objects'][] = $filterobject;

      } else {
        $table['objects'][] = array_replace(array_fill_keys(array_keys($headers), NULL) , array_intersect_key(get_object_vars($object), $headers));
      }
    }
    return $table;
  }

  function human_filesize($file, $decimals = 2) {
    $bytes = filesize($file);
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
  }

  function pr($array) {
    print "<pre><code>";
    print_r($array);
    print "</code></pre>";
  }

  function session_set($name, $value) {
    return $_SESSION[$name] = $value;
  }

  function session_get($name) {
    if(isset($_SESSION[$name]))
      return $_SESSION[$name];

    return false;
  }

  function session_null($name) {
    unset($_SESSION[$name]);
  }

  function array_filter2($arr, $filter) {
    if(is_array($filter)) {
      $filter = array_flip($filter);
    } else {
      $filter = array_flip(array($filter));
    }

    if(is_array($arr)) {
      return array_intersect_key($arr, $filter);
    } elseif(is_object($arr)) {
      $temparr = array();
      foreach(array_keys($filter) as $prop) {
        $temparr[$prop] = $arr->$prop;
      }
      return $temparr;
    }

    return false;
  }

  function mime_type($filename) {
    $finfo = finfo_open(FILEINFO_MIME_TYPE);

    if($result = finfo_file($finfo, $filename)) {
      finfo_close($finfo);
      return $result;
    }
    finfo_close($finfo);
    return false;
  }

  function csrf_field() {
    $rnd = base64_encode(md5(random_int(0, 999999999)));
    session_set('csrf_form', $rnd);
    echo "<input type='hidden' name='csrf_form' value='{$rnd}'>";
  }

  function isbrowser(){
    $agent = @$_SERVER['HTTP_USER_AGENT'];
    if (strpos($agent, 'Gecko') !== false) {
      return true;
    }
    if (strpos($agent, 'WebKit') !== false) {
      return true;
    }
    if (strpos($agent, 'Presto') !== false) {
      return true;
    }
    if (strpos($agent, 'Trident') !== false) {
      return true;
    }
    if (strpos($agent, 'Blink') !== false) {
      return true;
    }
    return false;
  }
