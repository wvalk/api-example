<?php

  namespace Omtech\Helper\Exception;

  class FileNotExist extends Exception {

    public function __construct() {
      header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
      echo '500';
    }

  }
